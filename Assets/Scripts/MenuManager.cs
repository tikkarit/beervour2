using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class MenuManager : MonoBehaviour
{
    public static MenuManager instance;

    public bool gameOver = false;
    [SerializeField]
    private TMP_Text scoreText , beerText, goScoreText, goBeerText;
    [SerializeField]
    private GameObject gameMenu, menu, gameOverMenu, guide;

    private int currentCoin, currentScore;

    private void Awake()
    {
        if (instance == null)
            instance = this;
    }
    // Start is called before the first frame update
    void Start()
    {

        scoreText.text = "Score " + 0;
        beerText.text = "" + 0;
        
    }

    public void IncreaseCoin()
    {
        currentCoin++;
        currentScore+=5;
        beerText.text = "" + currentCoin;
        scoreText.text = "Score " + currentScore;

    }

    public void IncreaseScore()
    {
        currentScore++;
        scoreText.text = "Score " + currentScore;
    }

    public void PlayBtn()
    {
        SoundManager.instance.PlayClick();
        SoundManager.instance.PlayMusic();
        menu.SetActive(false);
        gameMenu.SetActive(true);
        guide.SetActive(true);
    }

    public void StartGame()
    {
        guide.SetActive(false);
        Player.instance.StartMoving = true;
    }

    public void GameOver()
    {
        SoundManager.instance.PlayDeath();
        gameOver = true;
        gameMenu.SetActive(false);
        gameOverMenu.SetActive(true);
        goScoreText.text = "Score: " + currentScore;
        goBeerText.text = "Beers: " + currentCoin;

    }
    public void HomeBtn()
    {
        SoundManager.instance.PlayClick();
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
